package com.codegeeks.commentapp.model;

import java.util.Date;

/**
 * Created by Trevor Malovhele on 2015/06/24.
 */
public class Incident {

    String incidentType;
    String incidentDetails;
    Citizen postedBy;
    Location incidentLocation;
    Date incidentDate;


    public String getIncidentType() {
        return incidentType;
    }

    public void setIncidentType(String incidentType) {
        this.incidentType = incidentType;
    }

    public String getIncidentDetails() {
        return incidentDetails;
    }

    public void setIncidentDetails(String incidentDetails) {
        this.incidentDetails = incidentDetails;
    }

    public Citizen getPostedBy() {
        return postedBy;
    }

    public void setPostedBy(Citizen postedBy) {
        this.postedBy = postedBy;
    }

    public Location getIncidentLocation() {
        return incidentLocation;
    }

    public void setIncidentLocation(Location incidentLocation) {
        this.incidentLocation = incidentLocation;
    }

    public Date getIncidentDate() {
        return incidentDate;
    }

    public void setIncidentDate(Date incidentDate) {
        this.incidentDate = incidentDate;
    }

    @Override
    public String toString() {
        return "Incident{" +
                "incidentType='" + incidentType + '\'' +
                ", incidentDetails='" + incidentDetails + '\'' +
                ", postedBy=" + postedBy +
                ", incidentLocation=" + incidentLocation +
                ", incidentDate=" + incidentDate +
                '}';
    }
}
