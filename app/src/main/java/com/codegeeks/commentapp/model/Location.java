package com.codegeeks.commentapp.model;



/**
 * Created by Trevor Malovhele on 2015/06/18.
 */
public class Location {

    private String locationName;
    private String province;
    private String municipality;
    private Address address;
    private double longitude;
    private double latitude;

    public Location( String locationName, String province, String municipality, Address address, double longitude, double latitude) {
        this.locationName = locationName;
        this.province = province;
        this.municipality = municipality;
        this.address = address;
        this.longitude = longitude;
        this.latitude = latitude;
    }

    public String getLocationName() {
        return locationName;
    }

    public void setLocationName(String locationName) {
        this.locationName = locationName;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getMunicipality() {
        return municipality;
    }

    public void setMunicipality(String municipality) {
        this.municipality = municipality;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    @Override
    public String toString() {
        return "Location{" +
                ", locationName='" + locationName + '\'' +
                ", province='" + province + '\'' +
                ", city='" + municipality + '\'' +
                ", address=" + address +
                ", longitude=" + longitude +
                ", latitude=" + latitude +
                '}';
    }
}
