package com.codegeeks.commentapp.model;

/**
 * Created by Trevor Malovhele on 2015/06/24.
 */
public class Citizen {
    String citizenName;
    String cell_number;

    public String getCitizenName() {
        return citizenName;
    }

    public void setCitizenName(String citizenName) {
        this.citizenName = citizenName;
    }

    public String getCell_number() {
        return cell_number;
    }

    public void setCell_number(String cell_number) {
        this.cell_number = cell_number;
    }

    @Override
    public String toString() {
        return "Citizen{" +
                "citizenName='" + citizenName + '\'' +
                ", cell_number='" + cell_number + '\'' +
                '}';
    }
}
