package com.codegeeks.commentapp.model;

/**
 * Created by Trevor Malovhele on 2015/06/18.
 */
public class Address {

    private String streetNo;
    private String streetName;
    private String suburb;
    private String postCode;

    public Address(String streetNo, String streetName, String suburb, String postCode) {
        this.streetNo = streetNo;
        this.streetName = streetName;
        this.suburb = suburb;
        this.postCode = postCode;
    }

    public String getStreetNo() {
        return streetNo;
    }
    public void setStreetNo(String streetNo) {
        this.streetNo = streetNo;
    }

    public String getStreetName() {
        return streetName;
    }

    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    public String getSuburb() {
        return suburb;
    }

    public void setSuburb(String suburb) {
        this.suburb = suburb;
    }

    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    @Override
    public String toString() {
        return "Address{" +
                "streetNo=" + streetNo +
                ", streetName='" + streetName + '\'' +
                ", suburb='" + suburb + '\'' +
                ", postCode='" + postCode + '\'' +
                '}';
    }
}
