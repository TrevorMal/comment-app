package com.codegeeks.commentapp.services.Location;

import android.util.Log;

import com.codegeeks.commentapp.model.Location;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Trevor Malovhele on 2015/08/15.
 */
public class CommentSOAPServiceClient {


    private static final String REMOTE_URL = "http://192.168.43.173:9000/CommentApp.svc?wsdl";
    private static final String METHOD_NAME = "LoadComment";
    private static final String NAMESPACE = "http://tempuri.org/";
    private static final String SOAP_ACTION = "http://tempuri.org/ICommentApp/"+METHOD_NAME;

    public void testConnectionToService(){
        Log.i("SERVICE INFO", "In SOAP service method.");

        SoapObject object = new SoapObject(NAMESPACE,METHOD_NAME);
        SoapSerializationEnvelope  envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);

        envelope.dotNet = true;
        envelope.setOutputSoapObject(object);

        HttpTransportSE get = new HttpTransportSE(REMOTE_URL);
        String res = "";
        try{
            get.call(SOAP_ACTION,envelope);
            res = envelope.getResponse().toString();
            Log.i("SERVICE INFO", "Status: " + res);

            SoapObject soapObject = (SoapObject) envelope.getResponse();
            //SoapObject  obj = (SoapObject) soapObject.getProperty("LoadComment");

            Log.i("SERVICE INFO", "OBJECT: " + soapObject.getProperty("LoadComment"));
        }
        catch(Exception ex) {
            Log.e("SERVICE ERROR", ex.toString());
        }
    }
    public List<Location> getLocationFromDatabase(String postalCode){
        List<Location> locations = new ArrayList<>();

        return locations;
    }
}
