package com.codegeeks.commentapp.services;

import android.util.Log;

import com.codegeeks.commentapp.model.Incident;
import com.codegeeks.commentapp.model.Location;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;

import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

/**
 * Created by Trevor Malovhele on 2015/06/16.
 */
public class CommentServiceClient {
    //final String TEST_URL = "http://192.168.4.123:8045";
    final String URL = "http://197.96.138.247:8045";
    String result = "";
    //List<Location> location = new ArrayList<>();

    DefaultHttpClient client = new DefaultHttpClient();
    Gson gson = new Gson();
    ObjectMapper mapper = new ObjectMapper();
    public List<Location> getLocation() {
        List<Location> location = new ArrayList<>();
        Log.i("SERVICE INFO", "In service method.");
        HttpGet get = new HttpGet(URL +"/api/location/");
        try {
            HttpResponse response = client.execute(get);
            StatusLine statusLine = response.getStatusLine();
            int statusCode = statusLine.getStatusCode();
            Log.i("SERVICE INFO", "Status: " + statusCode);
            if (statusCode == 200) {
                InputStream stream = response.getEntity().getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
                String jsonString = reader.readLine();
                Log.i("SERVICE INFO", "Object: " + jsonString);

                location = mapper.readValue(jsonString, new TypeReference<List<Location>>() {});
                Log.i("SERVICE INFO", "JSONObject: " + location.get(0).getLocationName());
                stream.close();
            } else {
                Log.e("SERVICE ERROR", "Failed to connect.." + statusCode);
            }

            Log.i("SERVICE INFO", "Object: Location" + location);

        } catch (Exception e) {
            Log.e("SERVICE ERROR", e.toString(), e);
        }
        return location;
    }

    @POST
    @Path("/post")
    @Consumes("application/json")
    @Produces("application/json")
    private Response postComment_(Incident incident) {

        String result = "Query has been successfully added:" + incident;
        return Response.status(201).entity(result).build();
    }

    public  String postComment(Incident incident){
        String response = "Error";
        Log.i("SERVICE INFO", "JSON: " + gson.toJson(incident));
        try{
            StringEntity entity = new StringEntity(gson.toJson(incident));

            HttpPost post = new HttpPost(URL + "/api/incident/create");
            post.setEntity(entity);
            post.setHeader("Accept", "application/json");
            post.setHeader("Content-type", "application/json");

            Log.i("SERVICE INFO", "ENTITY: " + post.toString());

            ResponseHandler responseHandler = new BasicResponseHandler();
            response = client.execute(post,responseHandler).toString();

            Log.i("SERVICE INFO", "Response: " + response);
        }catch (IOException e){
            Log.e("SERVICE ERROR", e.getMessage() + "\n" + e.getCause(), e);
        }

        return response;

    }
}
