package com.codegeeks.commentapp;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.location.Address;
import android.location.Geocoder;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.ActionBarActivity;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import com.codegeeks.commentapp.model.Citizen;
import com.codegeeks.commentapp.model.Incident;
import com.codegeeks.commentapp.model.Location;
import com.codegeeks.commentapp.services.Location.CommentSOAPServiceClient;
import com.codegeeks.commentapp.services.Location.GPSTracker;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;


public class MainActivity extends ActionBarActivity {


    EditText message;
    EditText input;
    Button submitButton;
    Button closeButton;
    RadioGroup radioGroup;
    TextView heading;
    TextView current_location;
    String incidentType = "Comment";
    GPSTracker tracker;
    Geocoder geocoder;
    Spinner spinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        TelephonyManager mTelephonyMgr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        final Context context = this;
        //final CommentServiceClient client = new CommentServiceClient();
        final CommentSOAPServiceClient soapServiceClient = new CommentSOAPServiceClient();
        final AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
        final String cell_number = mTelephonyMgr.getDeviceId();

        List<String> services = new ArrayList<>();
        ArrayAdapter<String> adapter;
        services.add("Police Station");
        services.add("Hospital");
        services.add("Clinic");
        services.add("School");


        tracker = new GPSTracker(this);
        geocoder = new Geocoder(this, Locale.getDefault());

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
        adapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,services);


        //Test logging to get location from DB.
        //Log.i("TEST SERVER", "DB Location" + client.getLocation().toString());
        soapServiceClient.testConnectionToService();

        message = (EditText) findViewById(R.id.message);
        input = (EditText) findViewById(R.id.user_name);
        submitButton = (Button) findViewById(R.id.submit);
        closeButton = (Button) findViewById(R.id.close);
        radioGroup = (RadioGroup) findViewById(R.id.rgd_action);
        heading = (TextView) findViewById(R.id.location);
        closeButton.setVisibility(View.INVISIBLE);
        current_location = (TextView) findViewById(R.id.current_location);
        spinner = (Spinner) findViewById(R.id.service_spinner);

        //spinner.setOnItemClickListener((AdapterView.OnItemClickListener) this);
        spinner.setAdapter(adapter);

        Location location = null;


        Log.i("SERVICE INFO", "Cell Number" + mTelephonyMgr.getLine1Number());

        if (tracker.canGetLocation()) {
            //Log.i("LOCATION", tracker.getLocation().getProvider());
            double lon = tracker.getLongitude();
            double lat = tracker.getLatitude();
            try {
                List<Address> addresses = geocoder.getFromLocation(lat, lon, 1);
                if (addresses == null) {
                    location = new Location("", "", "", new com.codegeeks.commentapp.model.Address("", "", "", ""), 0.00, 0.00);
                } else {
                    for (Address ad : addresses) {
                        location = new Location(ad.getLocality(), ad.getCountryName(), ad.getCountryCode(), new com.codegeeks.commentapp.model.Address(ad.getFeatureName(), ad.getThoroughfare(), ad.getLocality(), ad.getPostalCode()), ad.getLongitude(), ad.getLatitude());
                        //Log.i("SERVICE INFO", "ADDRESS" + ad);
                    }
                }
            } catch (Exception e) {
                Log.e("SERVICE ERROR", "Locations convection" + e.getMessage());
                location = new Location("", "", "", new com.codegeeks.commentapp.model.Address("", "", "", ""), 0.00, 0.00);
            }
        } else {
            tracker.showSettingAlert();
        }
        try {

            current_location.setText(location.getAddress().getStreetNo() + " " + location.getAddress().getStreetName() + ", " + location.getLocationName() + " " + location.getAddress().getPostCode());
        }
        catch (NullPointerException ex){
            ex.printStackTrace();
            current_location.setText("Device location not found.");

        }
        final Location incidentLocation = location;

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                incidentType = ((RadioButton) findViewById(radioGroup.getCheckedRadioButtonId())).getText().toString();
                message.setHint("Please type your " + incidentType);
            }
        });

        dialogBuilder.setTitle("INFO")
                .setCancelable(false)
                .setIcon(android.R.drawable.ic_dialog_info)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        clearFields();
                        closeButton.setVisibility(View.VISIBLE);
                        submitButton.setVisibility(View.INVISIBLE);
                    }
                });

        submitButton.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                String serverResponse;
                Incident incident = new Incident();
                Citizen citizen = new Citizen();

                citizen.setCitizenName(input.getText().toString());
                citizen.setCell_number(getDeviceId());
                incident.setIncidentLocation(incidentLocation);
                incident.setPostedBy(citizen);
                incident.setIncidentDetails(message.getText().toString());
                incident.setIncidentType(incidentType);
                incident.setIncidentDate(new Date());

                /*Post the comment to the service.*/

                //serverResponse = client.postComment(incident);

                dialogBuilder.setMessage("Message: Thanks for the notification we will investigate further...");

                AlertDialog dialog = dialogBuilder.create();
                dialog.show();

            }
        });

        closeButton.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.this.finish();
            }
        });
    }

    private void clearFields() {
        message.setText("");
        input.setText("");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        if (id == R.id.action_clear) {
            clearFields();
            return true;
        }
        if (id == R.id.action_exit) {
            clearFields();
            MainActivity.this.finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public String getDeviceId(){

        //getting device ID
        TelephonyManager telManager =(TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
//        TextView phoneID =(TextView) findViewById(R.id.phoneID);
//        phoneID.setText("Device ID: " + telManager.getDeviceId());
//
//        TextView Phone_number =(TextView) findViewById(R.id.Phone_number);
//        phoneID.setText("Device ID: " + telManager.getDeviceId());
//
//
//
//        final TextView accountsData = (TextView) findViewById(R.id.accName);

        String possibleEmail="";

        try{
            possibleEmail += "Gmail Accounts\n\n";

            Account[] accounts =
                    AccountManager.get(this).getAccountsByType("com.google");

            possibleEmail = accounts[0].name;

            Log.i("SERVICE", "POSSIBLE EMAIL:"+ possibleEmail);

        }
        catch(Exception e)
        {
            Log.e("Exception", "Exception:"+e) ;
        }


        /*try{

            possibleEmail += "All Accounts\n\n";

            Account[] accounts = AccountManager.get(this).getAccounts();
            for (Account account : accounts) {

                possibleEmail += " Account Name: "+account.name+"Account Type : "+account.type+" , \n";
                possibleEmail += " \n";

            }
        }
        catch(Exception e)
        {
            Log.e("Exception", "Exception:"+e) ;
        }*/

        // Show on screen
        //accountsData.setText(possibleEmail);

        Log.e("Exception", "mails:"+possibleEmail) ;

        return possibleEmail;
    }
}
